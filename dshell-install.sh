#!/bin/sh

set -x

dshell_source=$(pwd)
dshell_directory=$HOME/.cache/dshell/

if [ ! -d $dshell_directory ]; then
    mkdir $dshell_directory
fi
for f in docker-compose.yml Dockerfile.centos Dockerfile.simple \
    Dockerfile.fedora entrypoint.sh; do

    sed -e 's/@USER@/'$USER'/g' \
        -e 's/@UID@/'$(id -u)'/g' \
        -e 's,@HOME@,'$HOME',g' \
        $dshell_source/$f > $dshell_directory/$f
done

if [ -d $HOME/bin ]; then
    install -m755 $dshell_source/dshell.sh $HOME/bin/dsh
fi
