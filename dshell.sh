#!/bin/sh

dshell_directory=$HOME/.cache/dshell/

docker=$(which docker)
docker_compose=$(which docker-compose)

if [ -n "$1" ]; then
    service=$1
    shift
else
    service=dev
fi

if [ "$1" = "--" ]; then
    shift
fi

SUDO=
ret=$($docker ps 2>&1)
if [ $? -ne 0 ] && echo $ret | grep -q 'permission denied'; then
    SUDO=sudo
fi

DOCKER="$SUDO $docker"
DC="$SUDO $docker_compose \
    --project-directory $dshell_directory \
    -f $dshell_directory/docker-compose.yml"

if ! $DC up -d $service; then
    echo "Unable to create docker stack"
    exit 1
fi

container=$($DC ps -q $service)
$DOCKER exec $container rm -f /var/run/nologin

ipaddr=$($DOCKER inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $container | head -1)

SSH="ssh \
    -o StrictHostKeyChecking=false \
    -o UserKnownHostsFile=/dev/null \
    -t -A"

tries=0
while [ $tries -lt 5 ]; do
    if echo | nc -w 1 $ipaddr 22 2> /dev/null | grep -q SSH-2; then
        break
    fi
    sleep 1
    tries=$((tries+1))
done

cmd="cd $(pwd); exec $SHELL"
if [ $# -gt 0 ]; then
    cmd="cd $(pwd); exec $@"
fi
if [ -z "$TMUX" ]; then
    exec $SSH $ipaddr tmux new-session -A
else
    exec $SSH $ipaddr $cmd
fi
